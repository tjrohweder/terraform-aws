data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.amazon-linux-2.id
  instance_type          = "t3.nano"
  key_name               = var.key_name
  vpc_security_group_ids = [var.bastion_sg]
  user_data              = file("files/bastion_user_data.tpl")
  subnet_id              = var.public_subnet[1]

  tags = {
    Name = "Bastion"
  }
}
