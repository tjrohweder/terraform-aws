variable "private_subnet" {
  type = list(string)
}

variable "vpc_id" {}
variable "vpc_cidr" {}
variable "cluster_name" {}
variable "workers_instance_type" {}
variable "key_name" {}
variable "autoscaling_group_name" {}
variable "control_plane_sg" {}
variable "sg_node" {}
variable "as_capacity" {}
variable "as_max_capacity" {}
variable "as_min_capacity" {}
