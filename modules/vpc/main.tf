data "aws_availability_zones" "available" {}

resource "aws_vpc" "k8s" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name                                        = "k8s"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_subnet" "private" {
  count                   = 2
  vpc_id                  = aws_vpc.k8s.id
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  cidr_block              = cidrsubnet(aws_vpc.k8s.cidr_block, 5, count.index + 1)
  map_public_ip_on_launch = false

  tags = {
    Name                                        = "private-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_subnet" "public" {
  count                   = 2
  vpc_id                  = aws_vpc.k8s.id
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  cidr_block              = cidrsubnet(aws_vpc.k8s.cidr_block, 8, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name                                        = "public-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.k8s.id

  tags = {
    Name = "IGW"
  }
}

resource "aws_eip" "nat_eips" {
  count = length(var.public_subnet)
  vpc   = true

  tags = {
    Name = "NAT-${count.index + 1}"
  }
}

resource "aws_nat_gateway" "nat" {
  count         = length(var.public_subnet)
  allocation_id = element(var.nat_eips, count.index)
  subnet_id     = element(var.public_subnet, count.index)

  tags = {
    Name = "NAT-${count.index + 1}"
  }
}

resource "aws_route_table" "private_route" {
  count  = length(var.private_subnet)
  vpc_id = aws_vpc.k8s.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = element(var.nat_gateway, count.index)

  }

  tags = {
    Name = "private-${count.index + 1}"
  }
}

resource "aws_route_table_association" "private_association" {
  count          = length(var.private_subnet)
  subnet_id      = element(var.private_subnet, count.index)
  route_table_id = element(var.private_route, count.index)
}

resource "aws_route_table" "public_route" {
  count  = length(var.public_subnet)
  vpc_id = aws_vpc.k8s.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public-${count.index + 1}"
  }
}

resource "aws_route_table_association" "public_association" {
  count          = length(var.public_subnet)
  subnet_id      = element(var.public_subnet, count.index)
  route_table_id = element(var.public_route, count.index)
}

resource "aws_security_group" "control_plane_sg" {
  name        = "terraform-eks-cluster"
  description = "Control-Plane communication with worker nodes"
  vpc_id      = aws_vpc.k8s.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name                                        = "sg-control-plane"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_security_group_rule" "eks-cluster-ingress-workstation-https" {
  cidr_blocks       = [var.vpc_cidr]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.control_plane_sg.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group" "sg_node" {
  name        = "terraform-sg_node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = aws_vpc.k8s.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name                                        = "terraform-sg_node"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_security_group_rule" "sg_node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.sg_node.id
  source_security_group_id = aws_security_group.sg_node.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "sg_node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_node.id
  source_security_group_id = aws_security_group.control_plane_sg.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.control_plane_sg.id
  source_security_group_id = aws_security_group.sg_node.id
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_security_group" "bastion_sg" {
  name        = "Bastion SG"
  description = "Allow SSH"
  vpc_id      = aws_vpc.k8s.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
