output "vpc_id" {
  value = aws_vpc.k8s.id
}

output "nat_eips" {
  value = aws_eip.nat_eips.*.id
}

output "private_subnet" {
  value = aws_subnet.private.*.id
}

output "public_subnet" {
  value = aws_subnet.public.*.id
}

output "nat_gateway" {
  value = aws_nat_gateway.nat.*.id
}

output "private_route" {
  value = aws_route_table.private_route.*.id
}

output "public_route" {
  value = aws_route_table.public_route.*.id
}

output "control_plane_sg" {
  value = aws_security_group.control_plane_sg.id
}
output "sg_node" {
  value = aws_security_group.sg_node.id
}
output "bastion_sg" {
  value = aws_security_group.bastion_sg.id
}
