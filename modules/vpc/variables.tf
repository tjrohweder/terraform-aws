variable "vpc_cidr" {}
variable "cluster_name" {}

variable "nat_eips" {
  type = list(string)
}

variable "public_subnet" {
  type = list(string)
}

variable "private_subnet" {
  type = list(string)
}

variable "nat_gateway" {
  type = list(string)
}

variable "private_route" {
  type = list(string)
}

variable "public_route" {
  type = list(string)
}

