## VPC
vpc_cidr = "172.32.0.0/16"

## EKS
cluster_name           = "production"
workers_instance_type  = "m5.large"
autoscaling_group_name = "eks-spot"
as_capacity            = 1
as_max_capacity        = 5
as_min_capacity        = 1
key_name               = "tjrohweder"
