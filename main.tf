terraform {
  required_version = "= 0.12.17"

  required_providers {
    aws = ">= 2.41.0"
  }

  backend "s3" {
    bucket  = "tjrohweder-eks-state"
    key     = "terraformtest.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/creds"
  profile                 = "default"
}

module "vpc" {
  source         = "./modules/vpc"
  vpc_cidr       = var.vpc_cidr
  nat_eips       = module.vpc.nat_eips
  private_subnet = module.vpc.private_subnet
  public_subnet  = module.vpc.public_subnet
  nat_gateway    = module.vpc.nat_gateway
  private_route  = module.vpc.private_route
  public_route   = module.vpc.public_route
  cluster_name   = var.cluster_name
}

module "eks" {
  source                 = "./modules/eks"
  cluster_name           = var.cluster_name
  private_subnet         = module.vpc.private_subnet
  vpc_id                 = module.vpc.vpc_id
  vpc_cidr               = var.vpc_cidr
  key_name               = var.key_name
  workers_instance_type  = var.workers_instance_type
  autoscaling_group_name = var.autoscaling_group_name
  control_plane_sg       = module.vpc.control_plane_sg
  sg_node                = module.vpc.sg_node
  as_capacity            = var.as_capacity
  as_max_capacity        = var.as_max_capacity
  as_min_capacity        = var.as_min_capacity
}

module "compute" {
  source        = "./modules/compute"
  bastion_sg    = module.vpc.bastion_sg
  key_name      = var.key_name
  public_subnet = module.vpc.public_subnet
}

