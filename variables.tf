## VPC
variable "vpc_cidr" {}

## EKS
variable "cluster_name" {}
variable "workers_instance_type" {}
variable "autoscaling_group_name" {}
variable "as_capacity" {}
variable "as_max_capacity" {}
variable "as_min_capacity" {}
variable "key_name" {}
